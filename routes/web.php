<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function() {
    
    Route::get('/home', 'ProductController@index')->name('home');
    
    Route::resource('product', 'ProductController');
    Route::get('/product/{product}/clone', [
        'as'            => 'product.clone', 
        'uses'          => 'ProductController@clone',
    ])->where(['product' => '[0-9]+']);

    Route::resource('item', 'ItemController');
    Route::get('/item/{item}/clone', [
        'as'            => 'item.clone', 
        'uses'          => 'ItemController@clone',
    ])->where(['item' => '[0-9]+']);
    
    Route::resource('fitting', 'FittingController');
    Route::resource('material', 'MaterialController');
    
    Route::get('/pdf/manufacturing', [
        'as'            => 'pdf.manufacturing', 
        'uses'          => 'PdfController@manufacturing',
    ]);
    Route::get('/pdf/price', [
        'as'            => 'pdf.price', 
        'uses'          => 'PdfController@price',
    ]);
});

Auth::routes(['register' => false]);
//Auth::routes();
