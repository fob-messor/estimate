<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Item;
use App\Fitting;
use App\Material;

class ProductController extends Controller
{
    
    protected $product;
    protected $item;
    protected $fitting;
    protected $material;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Product $product, Item $item, Fitting $fitting, Material $material)
    {
        $this->middleware('auth');
        
        $this->product  = $product;
        $this->item     = $item;
        $this->fitting  = $fitting;
        $this->material = $material;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('product-index', [
            'products' => $this->product->orderBy('in_pdf')->orderBy('sort')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product            = new Product;
        $product->name      = $request->name;
        $product->markup    = $request->markup;
        $product->in_pdf    = $request->in_pdf;
        $product->sort      = $request->sort;
        $product->fix_cost  = $request->fix_cost;
        $product->fix_price = $request->fix_price;
        $product->weight    = $request->weight;
        $product->save();

        return redirect()->route('product.show', ['id' => $product->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->findOrFail($id);

        return view('product-show', [
            'product'       => $product,
            'material'      => $this->material->orderBy('name','asc')->pluck('name','id'),
            'items'         => $product->items,
            'fittings'      => $product->fittings,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('product-edit', [
            'product' => $this->product->findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->product->findOrFail($id)->update([
            'name'      => $request->name,
            'markup'    => $request->markup,
            'in_pdf'    => $request->in_pdf,
            'sort'      => $request->sort,
            'fix_cost'  => $request->fix_cost,
            'fix_price' => $request->fix_price,
            'weight'    => $request->weight,
        ]);
        
        return redirect()->route('product.index');
    }

    public function clone($id)
    {
        
        $clone = $this->product->findOrFail($id);
        
        $product            = new Product;
        $product->name      = $clone->name . " CLONE";
        $product->markup    = $clone->markup;
        $product->fix_cost  = $clone->fix_cost;
        $product->fix_price = $clone->fix_price;
        $product->weight    = $clone->weight;

        $product->save();
        
        // Clone Items
        
        foreach ($clone->items as $item) {

            $clone_item                   = new Item;
            $clone_item->name             = $item->name;
            $clone_item->product_id       = $product->id;
            $clone_item->material_id      = $item->material_id;
            $clone_item->length           = $item->length;
            $clone_item->width            = $item->width;
            $clone_item->cut_length       = $item->cut_length;
            $clone_item->price_material   = $item->price_material;
            $clone_item->price_cut        = $item->price_cut;
            $clone_item->price            = $item->price;
            $clone_item->save();
            
        }
        
        // Clone Fittings
        
        foreach ($clone->fittings as $fitting) {
            
            $clone_fitting                = new Fitting;
            $clone_fitting->name          = $fitting->name;
            $clone_fitting->material      = $fitting->material;
            $clone_fitting->product_id    = $product->id;
            $clone_fitting->count         = $fitting->count;
            $clone_fitting->price         = $fitting->price;
            $clone_fitting->amount        = $fitting->amount;
            $clone_fitting->save();
            
        }
        
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = $this->product->findOrFail($id);
        
        // Delete Items
        foreach ($delete->items as $item) {
            $this->item->destroy($item->id);
        }
        
        // Delete Fittings
        foreach ($delete->fittings as $fitting) {
            $this->fitting->destroy($fitting->id);
        }
        
        $this->product->destroy($id);
        return redirect()->route('product.index');
    }
}
