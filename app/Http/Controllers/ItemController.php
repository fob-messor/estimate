<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Material;

class ItemController extends Controller
{
    protected $item;
    protected $material;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Item $item, Material $material)
    {
        $this->middleware('auth');
        
        $this->item  = $item;
        $this->material  = $material;        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $material       = $this->material->findOrFail($request->material_id);
        $price_material = ($request->length * $request->width) * ($material->price_material/1000000);
        $price_cut      = ($request->cut_length / 1000) * $material->price_cut;
        
        $item                   = new Item;
        $item->name             = $request->name;
        $item->product_id       = $request->product_id;
        $item->material_id      = $request->material_id;
        $item->length           = $request->length;
        $item->width            = $request->width;
        $item->cut_length       = $request->cut_length;
        $item->price_material   = $price_material;
        $item->price_cut        = $price_cut;
        $item->price            = $price_material + $price_cut;
        $item->save();

        return redirect()->route('product.show', ['id' => $request->product_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('item-edit', [
            'material' => $this->material->orderBy('name','asc')->pluck('name','id'),
            'item' => $this->item->findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $material       = $this->material->findOrFail($request->material_id);
        $price_material = ($request->length * $request->width) * ($material->price_material/1000000);
        $price_cut      = ($request->cut_length / 1000) * $material->price_cut;
        
        $this->item->findOrFail($id)->update([
            'name'              => $request->name,
            'material_id'       => $request->material_id,
            'length'            => $request->length,
            'width'             => $request->width,
            'cut_length'        => $request->cut_length,
            'price_material'    => $price_material,
            'price_cut'         => $price_cut,
            'price'             => $price_material + $price_cut
        ]);
   
        return redirect()->route('product.show', ['id' => $request->product_id]);
    }

    public function clone($id)
    {
        $clone = $this->item->findOrFail($id);

        $item                   = new Item;
        $item->name             = $clone->name . " CLONE";
        $item->product_id       = $clone->product_id;
        $item->material_id      = $clone->material_id;
        $item->length           = $clone->length;
        $item->width            = $clone->width;
        $item->cut_length       = $clone->cut_length;
        $item->price_material   = $clone->price_material;
        $item->price_cut        = $clone->price_cut;
        $item->price            = $clone->price;
        $item->save();

        return redirect()->route('product.show', ['id' => $clone->product_id]);        
    }    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->item->destroy($id);
        return back()->withInput();
    }
}
