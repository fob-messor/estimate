<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fitting;

class FittingController extends Controller
{
    protected $fitting;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Fitting $fitting)
    {
        $this->middleware('auth');
        
        $this->fitting  = $fitting;        
    }    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fitting                = new Fitting;
        $fitting->name          = $request->name;
        $fitting->material      = $request->material;
        $fitting->product_id    = $request->product_id;
        $fitting->count         = $request->count;
        $fitting->price         = $request->price;
        $fitting->amount        = $request->count * $request->price;
        $fitting->save();

        return redirect()->route('product.show', ['id' => $request->product_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('fitting-edit', [
            'fitting' => $this->fitting->findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->fitting->findOrFail($id)->update([
            'name'      => $request->name,
            'material'  => $request->material,
            'count'     => $request->count,
            'price'     => $request->price,
            'amount'    => $request->count * $request->price
        ]);
        
        return redirect()->route('product.show', ['id' => $request->product_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->fitting->destroy($id);
        return back()->withInput();
    }
}
