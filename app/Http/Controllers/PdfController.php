<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Product;
use App\Item;
use App\Fitting;
use App\Material;

class PdfController extends Controller
{

    protected $product;
    protected $item;
    protected $fitting;
    protected $material;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Product $product, Item $item, Fitting $fitting, Material $material)
    {
        $this->middleware('auth');
        
        $this->product  = $product;
        $this->item     = $item;
        $this->fitting  = $fitting;
        $this->material = $material;
    }
    
    public function manufacturing() 
    {
        $pdf  = PDF::loadView('pdf_manufacturing', [
            'date'      => date("Y.m.d"),
            'products'  => $this->product->where('in_pdf', 'Y')->orderBy('sort')->get()
        ]);

        return $pdf->download('fob_messor_estimate_manufacturing-' . time() . '.pdf');
    }

    public function price() 
    {
        $pdf  = PDF::loadView('pdf_price', [
            'date'      => date("Y.m.d"),
            'products'  => $this->product->where('in_pdf', 'Y')->orderBy('sort')->get()
        ]);

        return $pdf->download('fob_messor_estimate_price-' . time() . '.pdf');
    }    
}
