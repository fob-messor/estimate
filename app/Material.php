<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends Model
{
    use SoftDeletes;
        
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name', 'price_material', 'price_cut'
    ];
}
