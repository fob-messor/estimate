<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
        
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name', 'markup', 'in_pdf', 'sort', 'fix_cost', 'fix_price', 'weight'
    ];
    
    public function items() {
        return $this->hasMany('App\Item')->orderBy('name');
    }
    
    public function items_sum() {
        return round($this->hasMany('App\Item')->sum('price'));
    }

    public function items_price_material_sum() {
        return round($this->hasMany('App\Item')->sum('price_material'));
    }
    
    public function items_price_cut_sum() {
        return round($this->hasMany('App\Item')->sum('price_cut'));
    }

    public function items_length_sum() {
        return round($this->hasMany('App\Item')->sum('length'));
    }
    
    public function items_width_sum() {
        return round($this->hasMany('App\Item')->sum('width'));
    }    

    public function items_cut_length_sum() {
        return round($this->hasMany('App\Item')->sum('cut_length'));
    }
    
    public function fittings() {
        return $this->hasMany('App\Fitting')->orderBy('name');
    }
    
    public function fittings_sum() {
        return round($this->hasMany('App\Fitting')->sum('amount'));
    }
}
