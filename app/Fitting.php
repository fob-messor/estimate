<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fitting extends Model
{
    use SoftDeletes;
        
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name', 'material', 'product_id',  'count', 'price', 'amount'
    ];
}
