<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;
        
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name', 'product_id', 'material_id', 'length', 'width', 'cut_length', 'price_material', 'price_cut', 'price'
    ];
    
    public function product() {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }
    
    public function material() {
        return $this->hasOne('App\Material', 'id', 'material_id');
    }
    
    public function size(){
        return $this->length * $this->width;
    }
    
//    public function price_material(){
//            return $this->price_material;
//    }
    
//    public function price_cut(){
////        if($this->price_cut == 0) {
////            return ($this->cut_length / 1000) * $this->material->price_cut;
////        } else {
//            return $this->price_cut;
////        }
//    }
    
//    public function price(){
////        if($this->price == 0) {
////            return $this->price_material() + $this->price_cut();
////        } else {
//            return $this->price;
////        }
//    }
}
