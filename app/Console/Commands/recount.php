<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Product;
use App\Item;
use App\Fitting;
use App\Material;


class recount extends Command
{
    protected $product;
    protected $item;
    protected $fitting;
    protected $material;    
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recount';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Product $product, Item $item, Fitting $fitting, Material $material)
    {
        parent::__construct();
        
        $this->product  = $product;
        $this->item     = $item;
        $this->fitting  = $fitting;
        $this->material = $material;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = $this->product->all();
        
        foreach ($products as $product) {
            echo "Product name: " . $product->name;
            echo "\n";
            
            foreach ($product->items as $item) {
                $material       = $this->material->findOrFail($item->material_id);
                $price_material = ($item->length * $item->width) * ($material->price_material/1000000);
                $price_cut      = ($item->cut_length / 1000) * $material->price_cut;                
                                
                echo "     Item name: " . $item->name . " (old 'price_material': " . $item->price_material .", new 'price_material': " . $price_material . ")";
                echo "\n";
                
                $this->item->findOrFail($item->id)->update([
                    'price_material'    => $price_material,
                    'price_cut'         => $price_cut,
                    'price'             => $price_material + $price_cut
                ]);
            }
            echo "\n\n";
        }
    }
}
