@extends('layouts.app')

@section('content')
    <div class="card-body">
        
        <div class="page-header text-success">
          <h4>Редактирование материала</h4>      
        </div>
        <hr>

        {!! Form::model($material, [
            'method'    => 'PUT',
            'route'     => ['material.update', $material->id],
            'class'     => 'form-horizontal',
        ]) !!}

        <div class="form-group">
          <label>Наименование материала</label>
          {!! Form::text('name', null, array('class' => 'form-control', 'id' => 'material_name', 'required')) !!}
        </div>
        <div class="form-group">
          <label>Стоимость 1 кв.м.</label>
          {!! Form::text('price_material', null, array('class' => 'form-control', 'id' => 'material_price', 'required')) !!}
        </div>
        <div class="form-group">
          <label>Стоимость реза за 1 м.</label>
          {!! Form::text('price_cut', null, array('class' => 'form-control', 'id' => 'material_cut', 'required')) !!}
        </div>

        <div class="modal-footer">
            <a href="{{ URL::previous() }}" class="btn btn-secondary">Назад</a>
            {!! Form::submit('Сохранить изменения', array('class' => 'btn btn-primary')) !!}       
        </div>

        {!! Form::close() !!}

    </div>

@endsection
