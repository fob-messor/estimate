@extends('layouts.app')

@section('content')
    <div class="card-body">
        
        <div class="page-header text-success">
          <h4>Редактирование фурнитуры</h4>      
        </div>
        <hr>

        {!! Form::model($fitting, [
            'method'    => 'PUT',
            'route'     => ['fitting.update', $fitting->id],
            'class'     => 'form-horizontal',
        ]) !!}

        {{ Form::hidden('product_id', $fitting->product_id) }}                    

        <div class="form-group">
            <label>Наименование фурнитуры</label>
            {!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="form-group">
            <label>Материал фурнитуры</label>
            {!! Form::text('material', null, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="form-group">
            <label>Количество (шт.)</label>
            {!! Form::text('count', null, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="form-group">
            <label>Стоимость (за 1 шт.)</label>
            {!! Form::text('price', null, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="modal-footer">
            <a href="{{ URL::previous() }}" class="btn btn-secondary">Назад</a>
            {!! Form::submit('Сохранить изменения', array('class' => 'btn btn-primary')) !!}       
        </div>

        {!! Form::close() !!} 

    </div>

@endsection                    