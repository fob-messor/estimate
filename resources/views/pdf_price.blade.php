<!DOCTYPE html>
<html>
<head>
	<title>Стоимость продукции</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <style>
            body {
                font-family: DejaVu Sans, sans-serif;
                font-size: 12px;
            }
            table {
                border: 1px solid grey;
            }
            th {
                border: 1px solid grey;
                text-align: center;
            }
            td {
                border: 1px solid grey;
            }
        </style>
</head>

<body>
	<h2>Стоимость продукции</h2>
        
        <table cellspacing="1" width="90%">
                <thead>
                    <tr>
                        <th><i>Наименование</i></th>
                        <th width="25%"><i>Вес в граммах</i></th>
                        <th width="25%"><i>Стоимость</i></th>
                    </tr>
                </thead>

                <tbody>                    

            @foreach($products as $product)

                <tr>
                    <td><b>{{ $product->name }}</b></td>
                    <td>{{ $product->weight }}</td>
                    <td>{{ $product->fix_price }} р.</td>
                </tr>

            @endforeach

                </tbody>
            </table>
        
</body>
</html>