@extends('layouts.app')

@section('content')

    <div class="card-body">
        
        <div class="page-header text-success">
          <h4>Редактирование продукта: <b>{{ $product->name }}</b></h4>      
        </div>
        <hr>

        {!! Form::model($product, [
            'method'    => 'PUT',
            'route'     => ['product.update', $product->id],
            'class'     => 'form-horizontal',
        ]) !!}

        <div class="form-group">
          <label>Наименование продукта</label>
          {!! Form::text('name', null, array('class' => 'form-control', 'id' => 'product_name', 'required')) !!}
        </div>

        <div class="form-group">
          <label>Наценка (в %)</label>
          {!! Form::number('markup', null, array('class' => 'form-control', 'id' => 'product_name', 'required')) !!}
        </div>

        <div class="form-group">
          <label>Включить в PDF</label>
            {!! Form::select('in_pdf', [
                'N'   => 'Нет',
                'Y'   => 'Да'
            ], null, array('class' => 'form-control')) !!}
        </div>

        <div class="form-group">
          <label>Фиксированная стоимость изготовления</label>
          {!! Form::number('fix_cost', null, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="form-group">
          <label>Фиксированная стоимость продукта</label>
          {!! Form::number('fix_price', null, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="form-group">
          <label>Вес в граммах</label>
          {!! Form::number('weight', null, array('class' => 'form-control', 'required')) !!}
        </div>
        
        <div class="form-group">
          <label>Сортировка в PDF</label>
          {!! Form::number('sort', null, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="modal-footer">
            <a href="{{ URL::previous() }}" class="btn btn-secondary">Назад</a>
            {!! Form::submit('Сохранить изменения', array('class' => 'btn btn-primary')) !!}       
        </div>

        {!! Form::close() !!}

    </div>

@endsection

