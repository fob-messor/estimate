@extends('layouts.app')

@section('content')

    <div class="card-body">
        
        <div class="page-header text-success">
          <h4>Список продукции</h4>      
        </div>
        <hr>
        
        <a class="btn btn-success" href="#" data-toggle="modal" data-target="#ModalCreateProduct">Добавить</a>
        <a class="btn btn-primary" href="{{ route('pdf.manufacturing') }}">Стоимость изготовления продукции в PDF</a>
        <a class="btn btn-primary" href="{{ route('pdf.price') }}">Стоимость продукции в PDF</a>
        <hr>
        
        <table id="product-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th scope="col">Наименование</th>
                        <th scope="col">PDF</th>
                        <th scope="col">Материал и резка</th>
                        <th scope="col">Фурнитура</th>
                        <th scope="col">Себестоимость</th>
                        <th scope="col">Наценка</th>
                        <th scope="col">Цена</th>
                        <th scope="col">Прибыль</th>
                        <th scope="col">Вес</th>
                    </tr>
                </thead>

                <tbody>                    

            @foreach($products as $product)

                <tr>
                    <td>

                        <div class="btn-group">
                          <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('product.show', ['id' => $product->id]) }}"><i data-feather="file"></i> Состав</a>    
                            <a class="dropdown-item" href="{{ route('product.edit', ['id' => $product->id]) }}"><i data-feather="edit"></i> Редактировать</a>
                            <a class="dropdown-item" href="{{ route('product.clone', ['id' => $product->id]) }}"><i data-feather="copy"></i> Клонировать</a>
                            <div class="dropdown-divider"></div>

                            {!! Form::open(['method' => 'DELETE', 'route' => ['product.destroy', $product->id],'onsubmit' => 'return confirm("Вы действительно желаете удалить продукт?")']) !!}
                                <a class="dropdown-item" href="#" onclick="$(this).closest('form').submit();"><i data-feather="delete"></i> Удалить</a>
                            {!! Form::close() !!}

                            </a>
                          </div>
                        </div>

                        {{ $product->name }}</td>
                    <td>                                
                    @if($product->in_pdf == 'Y')
                        Есть
                    @else
                        Нет
                    @endif
                    </td>
                    <td>
                    @if($product->fix_cost != 0)
                        <b class="text-success">{{ $product->fix_cost }}</b>
                    @else
                        {{ $product->items_sum() }}
                    @endif
                    </td>
                    <td>{{ $product->fittings_sum() }}</td>
                    <td>
                    @if($product->fix_cost != 0)
                        <b class="text-success">{{ $product->fix_cost + $product->fittings_sum() }}</b>
                    @else
                        {{ $product->items_sum() + $product->fittings_sum() }}
                    @endif
                    </td>
                    <td>{{ $product->markup }}%</td>
                    <td>
                    @if($product->fix_cost != 0)
                        <b class="text-success">{{ ($product->fix_cost + $product->fittings_sum()) * ($product->markup / 100 + 1) }}</b>
                    @else
                        {{ ($product->items_sum() + $product->fittings_sum()) * ($product->markup / 100 + 1) }}
                    @endif
                    </td>
                    <td>
                    @if($product->fix_cost != 0)
                        <b class="text-success">{{ (($product->fix_cost + $product->fittings_sum()) * ($product->markup / 100 + 1)) - ($product->fix_cost + $product->fittings_sum()) }}</b>
                    @else
                        {{ (($product->items_sum() + $product->fittings_sum()) * ($product->markup / 100 + 1)) - ($product->items_sum() + $product->fittings_sum()) }}
                    @endif
                        
                        </td>
                    <td>{{ $product->weight }}</td>
                </tr>

            @endforeach

                </tbody>
            </table>

    </div>


<!-- ModalCreateProduct -->
<div class="modal fade" id="ModalCreateProduct" tabindex="-1" role="dialog" aria-labelledby="ModalCreateProductLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalCreateProductLabel">Добавить продукт</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
        {!! Form::open([
            'route'     => 'product.store',
            'class'     => 'form-horizontal',
            'method'    => 'POST'
        ]) !!}

        <div class="form-row">
            <div class="col">
                <label class="control-label">Наименование:</label>
                {!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
            </div>
            <div class="col">
                <label class="control-label">Наценка (в %):</label>
                {!! Form::number('markup', '50', array('class' => 'form-control', 'required')) !!}
            </div>
        </div>
        
        <div class="form-row">
            <div class="col">
                <label class="control-label">Включить в PDF:</label>
                {!! Form::select('in_pdf', [
                    'N'   => 'Нет',
                    'Y'   => 'Да'
                ], null, array('class' => 'form-control')) !!}
            </div>
            <div class="col">
                <label class="control-label">Сортировка в PDF:</label>
                {!! Form::number('sort', 0, array('class' => 'form-control', 'required')) !!}
            </div>
        </div>

        <div class="form-row">
            <div class="col">
                <label class="control-label">Фикс. стоимость изготовления:</label>
                {!! Form::number('fix_cost', 0, array('class' => 'form-control', 'required')) !!}
            </div>
            <div class="col">
                <label class="control-label">Фикс. стоимость продукта:</label>
                {!! Form::number('fix_price', 0, array('class' => 'form-control', 'required')) !!}
            </div>
            <div class="col">
                <label class="control-label">Вес в граммах:</label>
                {!! Form::number('weight', 0, array('class' => 'form-control', 'required')) !!}
            </div>            
        </div>
        
        <p></p>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
            {!! Form::submit('Добавить продукт', array('class' => 'btn btn-success')) !!}       
        </div>

        {!! Form::close() !!}          
          
      </div>
    </div>
  </div>
</div>

@endsection

@section('js-footer')

        $('#product-table').DataTable({
            "order": [[ 1, "asc" ]],
            "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "Все"] ],
            "language": {
              "search": "Фильтр:",
              "zeroRecords": "Нет данных для отображения",
              "sLengthMenu": "Отображать _MENU_ записей",
              "info": "Показаны записи _START_ - _END_, всего _TOTAL_ записей",
              "paginate": {
                "previous": "Назад",
                "next": "Дальше"
              }
            }
        });

@endsection