@extends('layouts.app')

@section('content')

    <div class="card-body">
        
        <div class="page-header text-success">
          <h4>Список материалов</h4>      
        </div>
        <hr>
        
        <a href="{{ URL::previous() }}" class="btn btn-secondary">Назад</a>
        <a class="btn btn-success" href="#" data-toggle="modal" data-target="#ModalCreateMaterial">Добавить</a>
        <hr>

        <table id="material-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th scope="col">Наименование материала</th>
                        <th scope="col">Стоимость 1 кв.м.</th>
                        <th scope="col">Стоимость реза за 1 м.</th>
                    </tr>
                </thead>

                <tbody>                    

            @foreach($materials as $material)

            <tr>
                <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('material.edit', ['id' => $material->id]) }}"><i data-feather="edit"></i> Редактировать</a>
                        <div class="dropdown-divider"></div>

                        {!! Form::open(['method' => 'DELETE', 'route' => ['material.destroy', $material->id],'onsubmit' => 'return confirm("Вы действительно желаете удалить материал?")']) !!}
                            <a class="dropdown-item" href="#" onclick="$(this).closest('form').submit();"><i data-feather="delete"></i> Удалить</a>
                        {!! Form::close() !!}

                        </a>
                      </div>
                    </div>

                    {{ $material->name }}</td>
                <td>{{ $material->price_material }}</td>
                <td>{{ $material->price_cut }}</td>
            </tr>

            @endforeach

                </tbody>
            </table>

    </div>

<!-- ModalCreateMaterial -->
<div class="modal fade" id="ModalCreateMaterial" tabindex="-1" role="dialog" aria-labelledby="ModalCreateMaterialLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalCreateMaterialLabel">Добавить материал</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        {!! Form::open([
            'route'     => 'material.store',
            'class'     => 'form-horizontal',
            'method'    => 'POST'
        ]) !!}

        <div class="form-row">
            <div class="col">
                <label class="control-label">Наименование материала:</label>
                {!! Form::text('name', null, array('class' => 'form-control', 'id' => 'material_name', 'required')) !!}
            </div>
        </div>
        
        <div class="form-row">
            <div class="col">
                <label class="control-label">Стоимость 1 кв.м.:</label>
                {!! Form::number('price_material', null, array('class' => 'form-control', 'id' => 'material_price', 'required')) !!}
            </div>
            <div class="col">
                <label class="control-label">Стоимость реза за 1 м.:</label>
                {!! Form::number('price_cut', null, array('class' => 'form-control', 'id' => 'material_cut', 'required')) !!}
            </div>
        </div>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
            {!! Form::submit('Добавить материал', array('class' => 'btn btn-primary')) !!}       
        </div>

        {!! Form::close() !!}          
          
      </div>
    </div>
  </div>
</div>

@endsection

@section('js-footer')

        $('#material-table').DataTable({
            "order": [[ 0, "asc" ]],
            "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "Все"] ],
            "language": {
              "search": "Фильтр:",
              "zeroRecords": "Нет данных для отображения",
              "sLengthMenu": "Отображать _MENU_ записей",
              "info": "Показаны записи _START_ - _END_, всего _TOTAL_ записей",
              "paginate": {
                "previous": "Назад",
                "next": "Дальше"
              }
            }
        });

@endsection