@extends('layouts.app')

@section('content')

    <div class="card-body">
        
        <div class="page-header text-success">
          <h4>Редактирование элемента</h4>      
        </div>
        <hr>

        {!! Form::model($item, [
            'method'    => 'PUT',
            'route'     => ['item.update', $item->id],
            'class'     => 'form-horizontal',
        ]) !!}

        {{ Form::hidden('product_id', $item->product_id) }}

        <div class="form-group">
            <label>Наименование элемента</label>
            {!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="form-group">
            <label>Материал:</label>
            {!! Form::select('material_id', $material, null, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="form-group">
            <label>Длина элемента (в мм)</label>
            {!! Form::text('length', null, array('class' => 'form-control', 'required')) !!}
        </div>                    

        <div class="form-group">
            <label>Ширина элемента (в мм)</label>
            {!! Form::text('width', null, array('class' => 'form-control', 'required')) !!}
        </div>

        <div class="form-group">
            <label>Длина реза (в мм)</label>
            {!! Form::text('cut_length', null, array('class' => 'form-control', 'required')) !!}
        </div>                    

        <div class="modal-footer">
            <a href="{{ URL::previous() }}" class="btn btn-secondary">Назад</a>
            {!! Form::submit('Сохранить элемент', array('class' => 'btn btn-primary')) !!}       
        </div>

        {!! Form::close() !!}                    

    </div>

@endsection
