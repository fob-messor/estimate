@extends('layouts.app')

@section('content')


    <div class="card-body">
        
        <div class="page-header text-success">
          <h4>Состав продукта: <b>{{ $product->name }}</b></h4>      
        </div>
        <hr>
        
        <a href="{{ URL::previous() }}" class="btn btn-secondary">Назад</a>
        <a class="btn btn-info" href="#" data-toggle="modal" data-target="#ModalCreateData">Данные</a>
        <a class="btn btn-success" href="#" data-toggle="modal" data-target="#ModalCreateItem">Добавить акриловый элемент</a>
        <a class="btn btn-success" href="#" data-toggle="modal" data-target="#ModalCreateFitting">Добавить фурнитуру</a>

        <hr>
        <div class="page-header text-secondary">
          <h4>Акриловые элементы</h4>
        </div>
        <hr>
        
            <table id="item-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th scope="col">Наименование</th>
                        <th scope="col">Материал</th>
                        <th scope="col">Длина/ширина</th>
                        <th scope="col">Стоимость акрила</th>
                        <th scope="col">Рез (метр)</th>
                        <th scope="col">Стоимость реза</th>
                        <th scope="col">Стоимость элемента</th>
                    </tr>
                </thead>

                <tbody>                    

            @foreach($items as $item)

                <tr>
                    <td>
                        
                        <div class="btn-group">
                          <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('item.edit', ['id' => $item->id]) }}"><i data-feather="edit"></i> Редактировать</a>
                            <a class="dropdown-item" href="{{ route('item.clone', ['id' => $item->id]) }}"><i data-feather="copy"></i> Клонировать</a>
                            <div class="dropdown-divider"></div>

                            {!! Form::open(['method' => 'DELETE', 'route' => ['item.destroy', $item->id],'onsubmit' => 'return confirm("Вы действительно желаете удалить элемент?")']) !!}
                                <a class="dropdown-item" href="#" onclick="$(this).closest('form').submit();"><i data-feather="delete"></i> Удалить</a>
                            {!! Form::close() !!}

                            </a>
                          </div>
                        </div>                        
                        
                        {{ $item->name }}</td>
                    <td>{{ $item->material->name }}</td>
                    <td>{{ $item->length }}/{{ $item->width }}</td>
                    <td>{{ $item->price_material }}</td>
                    <td>{{ $item->cut_length/1000 }}</td>
                    <td>{{ $item->price_cut }}</td>
                    <td>{{ $item->price }}</td>
                </tr>

            @endforeach

                </tbody>
            </table>

        <hr>
        <div class="page-header text-secondary">
          <h4>Фурнитура</h4>
        </div>
        <hr>        
        
            <table id="fitting-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th scope="col">Наименование</th>
                        <th scope="col">Материал</th>
                        <th scope="col">Кол-во</th>
                        <th scope="col">Цена за шт.</th>
                        <th scope="col">Стоимость</th>
                    </tr>
                </thead>

                <tbody>                    

            @foreach($fittings as $fitting)

                <tr>
                    <td>
                        
                        <div class="btn-group">
                          <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('fitting.edit', ['id' => $fitting->id]) }}"><i data-feather="edit"></i> Редактировать</a>
                            <div class="dropdown-divider"></div>

                            {!! Form::open(['method' => 'DELETE', 'route' => ['fitting.destroy', $fitting->id],'onsubmit' => 'return confirm("Вы действительно желаете удалить фурнитуру?")']) !!}
                                <a class="dropdown-item" href="#" onclick="$(this).closest('form').submit();"><i data-feather="delete"></i> Удалить</a>
                            {!! Form::close() !!}

                            </a>
                          </div>
                        </div>

                        {{ $fitting->name }}</td>
                    <td>{{ $fitting->material }}</td>
                    <td>{{ $fitting->count }}</td>
                    <td>{{ $fitting->price }} р.</td>
                    <td>{{ $fitting->amount }} р.</td>
                </tr>

            @endforeach

                </tbody>
            </table>
   
    </div>


<!-- ModalCreateItem -->
<div class="modal fade" id="ModalCreateItem" tabindex="-1" role="dialog" aria-labelledby="ModalCreateItemLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalCreateItemLabel">Добавить акриловый элемент</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        {!! Form::open([
            'route'     => 'item.store',
            'class'     => 'form-horizontal',
            'method'    => 'POST'
        ]) !!}

        {{ Form::hidden('product_id', $product->id) }}

        
        <div class="form-row">
            <div class="col">
                <label class="control-label">Наименование:</label>
                {!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
            </div>
            <div class="col">
                <label class="control-label">Материал:</label>
                {!! Form::select('material_id', $material, null, array('class' => 'form-control', 'required')) !!}
            </div>
        </div>

        <div class="form-row">
            <div class="col">
                <label class="control-label">Длина элемента (в мм):</label>
                {!! Form::number('length', null, array('class' => 'form-control', 'required')) !!}
            </div>
            <div class="col">
                <label class="control-label">Ширина элемента (в мм):</label>
                {!! Form::number('width', null, array('class' => 'form-control', 'required')) !!}
            </div>
        </div>

        <div class="form-row">
            <div class="col">
                <label class="control-label">Длина реза (в мм):</label>
                {!! Form::number('cut_length', null, array('class' => 'form-control', 'required')) !!}
            </div>
            <div class="col">
                <label class="control-label"></label>
            </div>
        </div>

        <p></p>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
            {!! Form::submit('Добавить элемент', array('class' => 'btn btn-primary')) !!}       
        </div>

        {!! Form::close() !!}          
        
      </div>
    </div>
  </div>
</div>

<!-- ModalCreateFitting -->
<div class="modal fade" id="ModalCreateFitting" tabindex="-1" role="dialog" aria-labelledby="ModalCreateFittingLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalCreateFittingLabel">Добавить фурнитуру</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        
        {!! Form::open([
            'route'     => 'fitting.store',
            'class'     => 'form-horizontal',
            'method'    => 'POST'
        ]) !!}

        {{ Form::hidden('product_id', $product->id) }}                    

        
        <div class="form-row">
            <div class="col">
                <label class="control-label">Наименование:</label>
                {!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
            </div>
            <div class="col">
                <label class="control-label">Материал:</label>
                {!! Form::text('material', null, array('class' => 'form-control', 'required')) !!}
            </div>
        </div>
        
        <div class="form-row">
            <div class="col">
                <label class="control-label">Количество (шт.):</label>
                {!! Form::number('count', null, array('class' => 'form-control', 'required')) !!}
            </div>
            <div class="col">
                <label class="control-label">Стоимость (за 1 шт.):</label>
                {!! Form::number('price', null, array('class' => 'form-control', 'required')) !!}
            </div>
        </div>        

        <p></p>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
            {!! Form::submit('Добавить фурнитуру', array('class' => 'btn btn-primary')) !!}       
        </div>

        {!! Form::close() !!}          
        
      </div>
    </div>
  </div>
</div>

<!-- ModalCreateData -->
<div class="modal fade" id="ModalCreateData" tabindex="-1" role="dialog" aria-labelledby="ModalCreateDataLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalCreateDataLabel">Сводные данные продукта: <b>{{ $product->name }}</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

          
        <div class="form-row">
            <div class="col">
                <label class="control-label">Материал и резка:</label>
                {{ $product->items_sum() }} р.
            </div>
            <div class="col">
                <label class="control-label">Фурнитура:</label>
                {{ $product->fittings_sum() }} р.
            </div>
            <div class="col">
                <label class="control-label">Себестоимость:</label>
                {{ $product->items_sum() + $product->fittings_sum() }} р.
            </div>            
        </div>
          
        <div class="form-row">
            <div class="col">
                <label class="control-label">Общая длина:</label>
                {{ $product->items_length_sum() }} мм.
            </div>
            <div class="col">
                <label class="control-label">Общая ширина:</label>
                {{ $product->items_width_sum() }} мм.
            </div>
            <div class="col">
                <label class="control-label">Общая цена материала:</label>
                {{ $product->items_price_material_sum() }} р.
            </div>            
        </div>

        <div class="form-row">
            <div class="col">
                <label class="control-label">Общая длина реза:</label>
                {{ $product->items_cut_length_sum()/1000 }} м.
            </div>
            <div class="col">
                <label class="control-label">Общая стоимость реза:</label>
                {{ $product->items_price_cut_sum() }} р.
            </div>
            <div class="col">
                <label class="control-label"></label>
                
            </div>            
        </div>          
        
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>     
        </div>        
        
      </div>
    </div>
  </div>
</div>

@endsection

@section('js-footer')

        $('#item-table').DataTable({
            "order": [[ 0, "asc" ]],
            "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "Все"] ],
            "language": {
              "search": "Фильтр:",
              "zeroRecords": "Нет данных для отображения",
              "sLengthMenu": "Отображать _MENU_ записей",
              "info": "Показаны записи _START_ - _END_, всего _TOTAL_ записей",
              "paginate": {
                "previous": "Назад",
                "next": "Дальше"
              }
            }
        });

        $('#fitting-table').DataTable({
            "order": [[ 0, "asc" ]],
            "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "Все"] ],
            "language": {
              "search": "Фильтр:",
              "zeroRecords": "Нет данных для отображения",
              "sLengthMenu": "Отображать _MENU_ записей",
              "info": "Показаны записи _START_ - _END_, всего _TOTAL_ записей",
              "paginate": {
                "previous": "Назад",
                "next": "Дальше"
              }
            }
        });        
        
@endsection