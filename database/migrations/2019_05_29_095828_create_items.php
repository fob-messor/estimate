<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('product_id')->default('0');
            $table->integer('material_id')->default('0');
            $table->integer('length')->default('0');
            $table->integer('width')->default('0');
            $table->integer('cut_length')->default('0');
            $table->double('price_material', 10, 4)->default('0');
            $table->double('price_cut', 10, 4)->default('0');
            $table->double('price', 10, 4)->default('0');            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
